<?php

namespace SearchBundle\Services;

use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Description of SolrSchamaGeneratorService
 *
 * @author tgal
 */
class SolrSchamaGeneratorService {
    
    /**
     *
     * @var DomDocument $xmlDoc 
     */
    protected $xmlDoc;
    
    /**
     *
     * @var DomNode $placeholder; 
     */
    protected $placeholder;
    protected $schema;
    
    /**
     *
     * @var array 
     */
    protected $options;
    
    public function __construct($options = array()) {
        $resolver = new OptionsResolver();
        $resolver->setDefaults(array(
              'inputFileName' => __DIR__ . '/../Resources/schemaTemplate.xml',
            'outputFileName' => __DIR__ . '/../Resources/schema.xml',
        ));

        $this->options = $resolver->resolve($options);
        
        
    }


    public function generate($schema) {
        $this->loadXMLTemplate();
        foreach($schema as $field) {
           
            $this->processField($field);
        }
        
        $this->cleanup();
        $this->save();
        
    }
    
    protected function processField(array $field) {
      
        $newNode = $this->xmlDoc->createElement('field');
        $newNode->setAttribute('name', $field['name']);
        $newNode->setAttribute('type', $field['type']);
        $newNode->setAttribute('indexed', $field['indexed'] ? "true":"false");
        $newNode->setAttribute('stored',  $field['stored'] ? "true":"false");
        if(isset($field["required"])) {
            $newNode->setAttribute('required',  $field['required'] ? "true":"false");
        }
        if(isset($field["multiValued"])) {
            $newNode->setAttribute('multiValued',  $field['multiValued'] ? "true":"false");
        }

        $this->schema->insertBefore($newNode, $this->placeholder);
        $this->schema->insertBefore(new \DOMText("\n"), $this->placeholder);
         
    }
    
    protected function loadXMLTemplate() {
       libxml_use_internal_errors(true);
        $this->xmlDoc = new \DOMDocument();
        $this->xmlDoc->load($this->options['inputFileName']);
        $this->placeholder = $this->xmlDoc->getElementsByTagName('fieldplaceholder')->item(0);
        $this->schema = $this->xmlDoc->getElementsByTagName('schema')->item(0);
       
        
    }
    protected function cleanup(){
       $this->schema->removeChild($this->placeholder);
        
    }
    
    protected function save(){
        return $this->xmlDoc->save($this->options['outputFileName']);
    }
    
}
