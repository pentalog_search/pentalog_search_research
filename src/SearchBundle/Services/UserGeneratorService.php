<?php
namespace SearchBundle\Services;

use SearchBundle\Entities\SolrDocumentEntity;

class UserGeneratorService{
    private $chars;
    private $entityMethods;
    
    public function generateUsers($usersToGenerate=100){
        $yamlData = \Symfony\Component\Yaml\Yaml::parse(
                file_get_contents(__DIR__ . '/../Resources/config/fields.yml'));
        
        $users = array();
        $methods = $this->getEntityMethods($yamlData['solrFields']);
        var_dump($methods);
        for($i = 0; $i < $usersToGenerate; $i++){
            $user = new SolrDocumentEntity();
            
            foreach ($methods as $method => $field){
                $user->$method($this->generateFieldValue($yamlData['solrFields'][$field]));
            }
        }
        var_dump($user);
    }
    
    private function getEntityMethods($fieldsConfig){
        if(!is_array($this->entityMethods)){
            $this->entityMethods = array();
            
            foreach ($fieldsConfig as $field => $meta){
                $method = 'set'.ucfirst($field);
                $this->entityMethods[$method] = $field;
            }
        }
        
        return $this->entityMethods;
    }
    
    private function generateFieldValue($fieldMeta){
        $size = isset($fieldMeta['size'])?$fieldMeta['size']:3;
        
        switch ($fieldMeta['type']){
            case 'int':
                return $this->generateInt($size);
                break;
            case 'string':
                return $this->generateString($size);
                break;
            case 'date':
                return $this->generateDate($fieldMeta['min'], $fieldMeta['max']);
                break;
            case 'boolean':
                return $this->generateBoolean();
                break;
        }
    }
    
    private function generateDate($minYears, $maxYears){
        $yearSeconds = 31536000;
        $minSec = $yearSeconds * $minYears;
        $maxSec = $yearSeconds * $maxYears;
        $time = time() - rand($minSec, $maxSec);
        return new \DateTime('@'.$time);
    }
    
    private function generateBoolean(){
        return rand(0, 1);
    }

    private function generateInt($size=3){
        if($size==1){
            $min = 0;
        }
        else{
            $min = pow(10, $size - 1);
        }
        $max = pow(10, $size) - 1;
        
        return rand($min, $max);
    }
    
    private function getChars(){
        if(!is_array($this->chars)){
            $chars = range(48, 57);
            $chars = array_merge($chars, range(65, 90));
            $chars = array_merge($chars, range(97, 122));

            $chars = array_flip($chars);
            foreach ($chars as $hex => $foo){
                $chars[$hex] = chr($hex);
            }
            $this->chars = $chars;
        }
        
        return $this->chars;
    }

    private function generateString($size=10){
        $length = rand(ceil($size/2), $size);
        
        $chars = $this->getChars();
        $value = '';
        for($i = 0; $i < $length; $i++){
            $tmp = array_rand($chars);
            $value .= $chars[$tmp];
        }
        
        return $value;
    }
}