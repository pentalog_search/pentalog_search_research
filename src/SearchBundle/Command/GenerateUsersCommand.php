<?php
namespace SearchBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use SearchBundle\Services\UserGeneratorService;

class GenerateUsersCommand extends Command
{
    private $userGenarator;
    
    public function __construct(UserGeneratorService $service) {
        $this->userGenarator = $service;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('MeeticSearch:GenerateUsers')
            ->setDescription('Generate users in Oracle DB')
            ->addArgument(
                'count',
                InputArgument::REQUIRED,
                'the number of users to generate'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $count = $input->getArgument('count');
        $output->writeln("Generating $count users ...");
        $this->userGenarator->generateUsers($count);
        $output->writeln("Generated $count users");
    }
}