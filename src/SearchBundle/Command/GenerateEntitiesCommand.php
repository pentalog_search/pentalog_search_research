<?php
namespace SearchBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Config\Loader\FileLoader;
use Symfony\Component\Yaml\Yaml;
use SearchBundle\Entities;


/**
 * Description of GenerateEntitiesCommand
 *
 * @author cdad
 */
class GenerateEntitiesCommand  extends ContainerAwareCommand
{
 
    private $fieldsConfiguration;
    
    private function loadConfiguration()
    {
        $configValues = Yaml::parse(file_get_contents(__DIR__.'/../Resources/config/fields.yml'));
        return $configValues;
    }

    protected function configure() 
    {
        $this
            ->setName('MeeticSearch:GenerateEntities')
            ->setDescription('Generate Entities');
        $this->fieldsConfiguration = $this->loadConfiguration();
    }
    
    public function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $entityFile = fopen(__DIR__."/../Entities/SolrDocumentEntity.php", "w");
        } catch (Exception $ex) {
            $output->writeln("Cannot create entity file, message:".$ex->getMessage());
            throw new Exception("Cannot create entity file, message:".$ex->getMessage());
        }
        
        $entityCode = "<?php".PHP_EOL;
        $entityCode .= "namespace SearchBundle\Entities;".PHP_EOL.PHP_EOL;
        $entityCode .= "/**".PHP_EOL.
                "* Description of SolrDocumentEntity".PHP_EOL.
                "*".PHP_EOL.
                "* @author cdad".PHP_EOL.
                "*/".PHP_EOL.
                "class SolrDocumentEntity".PHP_EOL.
                "{".PHP_EOL;
        
        $properties = "";
        $methods = "";
        
        foreach($this->fieldsConfiguration['solrFields'] as $key => $value) {
            $properties .= '    private $'.$value["name"].';'.PHP_EOL;
            $methods .= '    public function set'.ucfirst($value["name"]).'($value)'.PHP_EOL.
                    '    {'.PHP_EOL.
                    '        $this->'.$value["name"].' = $value;'.PHP_EOL.
                    '    }'.PHP_EOL.
                    '    public function get'.ucfirst($value["name"]).'()'.PHP_EOL.
                    '    {'.PHP_EOL.
                    '        return $this->'.$value["name"].';'.PHP_EOL.
                    '    }'.PHP_EOL;
        }
        $entityCode .= $properties.PHP_EOL.$methods.'}';
        fwrite($entityFile, $entityCode);
        fclose($entityFile);
        
        $output->writeln("Entity generated");
        
        
    }
}
