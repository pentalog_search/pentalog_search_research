<?php
namespace SearchBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


/**
 * Description of GenerateSolrSchemaCommand
 *
 * @author cdad
 */
class GenerateSolrSchemaCommand  extends ContainerAwareCommand {
    
    protected function configure() 
    {
        $this
            ->setName('MeeticSearch:generateSolrSchema')
            ->setDescription('Generate SolrSchema ')
           
        ;
    }
    
    public function execute(InputInterface $input, OutputInterface $output)
    {
        
        $options = array(
            'inputFileName' => __DIR__ . '/../Resources/schemaTemplate.xml',
            'outputFileName' => __DIR__ . '/../Resources/schema.xml',
        );
        $service = new  \SearchBundle\Services\SolrSchamaGeneratorService($options);
        $yamlData = \Symfony\Component\Yaml\Yaml::parse( file_get_contents(__DIR__ . '/../Resources/config/fields.yml'));
       
        $service->generate($yamlData['solrFields']);
       
    }
}
